package application;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.DirectoryChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Controller implements Initializable {

	@FXML
	private BorderPane mainPane;
	@FXML
	private ScrollPane filesPane;
	private FlowPane flowPane;
	@FXML
	private TextField search;
	@FXML
	private MenuButton sortBtn;
	@FXML
	private Button logoutBackBtn;
	
	private Stage appStage;
	private boolean multiSelection = false;
	private VBox lastVb = null;
	private List<VBox> selectedVbs = null;
	private final Background focusBackground = new Background( new BackgroundFill(  Color.rgb(65, 107, 175), CornerRadii.EMPTY, Insets.EMPTY ) );
    private final Background unfocusBackground = new Background( new BackgroundFill( Color.rgb(244, 244, 244), CornerRadii.EMPTY, Insets.EMPTY ) );
	private Node prevPane;
	private List<Node> imagePanes = new ArrayList<>();
	private List<String> folderPaths = new ArrayList<>();
	private String currentFolder;
	private String mainPath = "/home/timos/Dropbox/Photos/smth/";
	
	
	public Controller(Stage appStage) {
		this.appStage = appStage;
	
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		flowPane = new FlowPane();
		imagePanes.add(flowPane);
		setGlobalEventHandler(search);        
		flowPane.setAlignment(Pos.TOP_LEFT);
		flowPane.setPadding(new Insets(15,15,15,15));
		appStage.widthProperty().addListener((obs, oldVal, newVal) -> {
			filesPane.setFitToWidth(true);
		    flowPane.setPrefWidth(filesPane.getWidth());
		});
		
		filesPane.setHbarPolicy(ScrollBarPolicy.NEVER);
		filesPane.setVbarPolicy(ScrollBarPolicy.AS_NEEDED);
		fadeEffect(250, filesPane);
		filesPane.setContent(flowPane);
		
		currentFolder = mainPath;
		folderPaths.add(currentFolder);
		File folder = new File(currentFolder);
		
		Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (final File file: folder.listFiles()) {
					VBox vb = new VBox();
					vb.setPadding(new Insets(5,5,5,5));
		            vb.setFocusTraversable(true);
		            vb.setAlignment(Pos.CENTER);
		            vb.setStyle("-fx-cursor: hand ;");
		            System.out.println(file.getPath());
		            if (file.isDirectory()) {
		            	Image img = new Image(getClass().getResource("folder.png").toExternalForm(),40.0,40.0,false,false);
				        ImageView imv = new ImageView(img);
				        Text desc = new Text(60,10,file.getName());
				        desc.setWrappingWidth(60);
				        vb.getChildren().addAll(imv,desc);
				        
				        folderEffects(filesPane,vb);
				        fadeEffect(250, vb);
			            Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								flowPane.getChildren().add(0,vb);
							}
						});
		            }
		            else if(file.getName().endsWith("jpg") || file.getName().endsWith("png")) {
			            Image img = new Image(file.toURI().toString(),40.0,40.0,false,false);
			            ImageView imv = new ImageView(img);
			            Text desc = new Text(60,10,file.getName());
			            desc.setWrappingWidth(60);
			            vb.getChildren().addAll(imv,desc);
			            
			            imageEffects(filesPane,vb);
			            
			            fadeEffect(250, vb);
			            Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								flowPane.getChildren().add(vb);
							}
						});
			    		
		            }
				}
			}
		});
		thread.start();
		
		filesPane.setOnKeyPressed(new  EventHandler<KeyEvent>() {
		@Override
		public void handle(KeyEvent event) {
			if (event.getCode() == KeyCode.CONTROL) {
				if (!multiSelection) {
					System.out.println("Multiselection ON");
					if (lastVb != null) {
						lastVb.setBackground(unfocusBackground);
						lastVb = null;
					}
					multiSelection = true;
					selectedVbs = new ArrayList<>();
				}
				else {
					System.out.println("Multiselection OFF");
					multiSelection = false;
						if (selectedVbs != null) {
						for (VBox vb : selectedVbs) {
							vb.setBackground(unfocusBackground);
							//TODO release data
						}
						selectedVbs = null;
					}
				}
			}
		}
	});
		
	}

	private void fetchFiles(VBox vbf) {
		flowPane = new FlowPane();
    	imagePanes.add(flowPane);
    	filesPane.setContent(flowPane);
    	Text folderName = (Text)vbf.getChildren().get(1);
    	currentFolder = currentFolder+ folderName.getText()+"/";
    	folderPaths.add(currentFolder);
    	logoutBackBtn.setText("Back");
		File folder = new File(currentFolder);
    	Thread thread = new Thread(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (final File file: folder.listFiles()) {
					VBox vb = new VBox();
					vb.setPadding(new Insets(5,5,5,5));
		            vb.setFocusTraversable(true);
		            vb.setAlignment(Pos.CENTER);
		            vb.setStyle("-fx-cursor: hand ;");
		            System.out.println(file.getPath());
		            if (file.isDirectory()) {
		            	Image img = new Image(getClass().getResource("folder.png").toExternalForm(),40.0,40.0,false,false);
				        ImageView imv = new ImageView(img);
				        Text desc = new Text(60,10,file.getName());
				        desc.setWrappingWidth(60);
				        vb.getChildren().addAll(imv,desc);
				        //folderPaths.add(e)
				        folderEffects(filesPane,vb);
				        fadeEffect(500, vb);
			            Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								flowPane.getChildren().add(0,vb);
							}
						});
			    		
		            }
		            else if(file.getName().endsWith("jpg") || file.getName().endsWith("png")) {
			            Image img = new Image(file.toURI().toString(),40.0,40.0,false,false);
			            ImageView imv = new ImageView(img);
			            Text desc = new Text(60,10,file.getName());
			            desc.setWrappingWidth(60);
			            vb.getChildren().addAll(imv,desc);
			            
			            imageEffects(filesPane,vb);
			            
			            fadeEffect(500, vb);
			            Platform.runLater(new Runnable() {
							
							@Override
							public void run() {
								// TODO Auto-generated method stub
								flowPane.getChildren().add(vb);
							}
						});
			    		
		            }
				}
			}
		});
		thread.start();
	}
	
	private void folderEffects(Node parent,VBox vb) {
		final ContextMenu contextMenu = new ContextMenu();
		MenuItem open = new MenuItem("Open");
        MenuItem move = new MenuItem("Move to...");
        MenuItem copy = new MenuItem("Copy to...");
        MenuItem delete = new MenuItem("Delete");
        contextMenu.getItems().addAll(open,copy, move, delete);
                              
		vb.setBackground(unfocusBackground);
        
        vb.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
            	if(mouseEvent.getButton().equals(MouseButton.SECONDARY) && !multiSelection){
            		//if (!vb.isFocused()) {
            			vb.requestFocus();
            			if (lastVb != null && lastVb != vb) {
            				lastVb.setBackground(unfocusBackground);
            				vb.setBackground(focusBackground);
            			}
            			else vb.setBackground(focusBackground);
            			lastVb = vb;
            		//}
            	}
            	else if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            		if (multiSelection) {
            			//if (!vb.isFocused()) {
            				if (vb.getBackground() == unfocusBackground) {
		            			vb.setBackground(focusBackground);
		            			selectedVbs.add(vb);
		            			System.out.println("Added to list");
            				}
            				else {
            					vb.setBackground(unfocusBackground);
		            			selectedVbs.remove(vb);
		            			System.out.println("Removed from list");
            				}

            				//}
            		}
            		else {
	            		//if (!vb.isFocused()) {
	            			vb.requestFocus();
	            			if (lastVb != null) {
	            				lastVb.setBackground(unfocusBackground);
	            			}
	            			lastVb = vb;
	            			vb.setBackground(focusBackground);
	            		//}
	            		if(mouseEvent.getClickCount() == 2){
	                        System.out.println("Double clicked");
	                        System.out.println("Double clicked");
	                        try {
	                        	fetchFiles(vb);
	                        	
	                        } catch(Exception e) {
	                        	e.printStackTrace();
	                        }
	                    }
            		}
                }
            }
        });
        
        vb.addEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED, event -> {
        	if (!multiSelection) {
                delete.setOnAction(new EventHandler<ActionEvent>() {
        			@Override
        			public void handle(ActionEvent event) {
        				vb.setBackground(unfocusBackground);
        				String pathToDelete = null;
        				int i=0;
        				boolean deleted = false;
        				Text filename = (Text)vb.getChildren().get(1);
        				
        				String name = currentFolder+filename.getText();
        				File f = new File(name);
        				if(f.delete()) {
        					System.out.println("Deleted");
        					flowPane.getChildren().remove(vb);
        				}
        				}
        		});
        		move.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("Cut...");
                    }
                });
                copy.setOnAction(new EventHandler<ActionEvent>() {

        			@Override
        			public void handle(ActionEvent arg0) {
        				// TODO Auto-generated method stub
        				Stage stage = new Stage();
        				stage.initOwner(appStage);
        				stage.initModality(Modality.WINDOW_MODAL);
        				DirectoryChooser directoryChooser = new DirectoryChooser();
        		        File selectedDirectory = 
        		                 directoryChooser.showDialog(stage);
        		         
        		        if(selectedDirectory == null){
        		            //labelSelectedDirectory.setText("No Directory selected");
        		        }else{
        		            //labelSelectedDirectory.setText(selectedDirectory.getAbsolutePath());
        		        }
        		        //stage.show();
        			}
        		});
        	}
        	else {
        		System.out.println("Global effect");
        	}
        	contextMenu.show(parent, event.getScreenX(), event.getScreenY());
            event.consume();
        });
        vb.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            contextMenu.hide();
        });
        //if user clicks elsewhere hide menu
        parent.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
        	if (lastVb != null && parent.isFocused()) lastVb.setBackground(unfocusBackground);
            contextMenu.hide();
        });
	}
        
		
	private void imageEffects(Node parent,VBox vb) {
		final ContextMenu contextMenu = new ContextMenu();
		MenuItem open = new MenuItem("Open");
        MenuItem move = new MenuItem("Move to...");
        MenuItem copy = new MenuItem("Copy to...");
        MenuItem delete = new MenuItem("Delete");
        contextMenu.getItems().addAll(open,copy, move, delete);
                              
		vb.setBackground(unfocusBackground);
        
        vb.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
            	if(mouseEvent.getButton().equals(MouseButton.SECONDARY) && !multiSelection){
            		//if (!vb.isFocused()) {
            			vb.requestFocus();
            			if (lastVb != null && lastVb != vb) {
            				lastVb.setBackground(unfocusBackground);
            				vb.setBackground(focusBackground);
            			}
            			else vb.setBackground(focusBackground);
            			lastVb = vb;
            		//}
            	}
            	else if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            		if (multiSelection) {
            			//if (!vb.isFocused()) {
            				if (vb.getBackground() == unfocusBackground) {
		            			vb.setBackground(focusBackground);
		            			selectedVbs.add(vb);
		            			System.out.println("Added to list");
            				}
            				else {
            					vb.setBackground(unfocusBackground);
		            			selectedVbs.remove(vb);
		            			System.out.println("Removed from list");
            				}

            				//}
            		}
            		else {
	            		//if (!vb.isFocused()) {
	            			vb.requestFocus();
	            			if (lastVb != null) {
	            				lastVb.setBackground(unfocusBackground);
	            			}
	            			lastVb = vb;
	            			vb.setBackground(focusBackground);
	            		//}
	            		if(mouseEvent.getClickCount() == 2){
	                        System.out.println("Double clicked");
	                        try {
		                        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("show_image.fxml"));
		                        Parent root = fxmlLoader.load();
		                        Stage stage = new Stage();
		                        stage.initOwner(appStage);
		        				stage.initModality(Modality.WINDOW_MODAL);
		                        Scene scene = new Scene(root);
		                        scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		                        stage.setScene(scene);
		                        stage.setResizable(false);
		                        ImageView imgv = (ImageView) scene.lookup("#displayedImage");
		                        Text imgName = (Text)vb.getChildren().get(1);
		                        Image img = new Image("file:"+currentFolder+ imgName.getText());
		                        imgv.setImage(img);
		                        stage.show();
	                        } catch(Exception e) {
	                        	e.printStackTrace();
	                        }
	                    }
            		}
                }
            }
        });
        
        vb.addEventHandler(ContextMenuEvent.CONTEXT_MENU_REQUESTED, event -> {
        	if (!multiSelection) {
                delete.setOnAction(new EventHandler<ActionEvent>() {
        			@Override
        			public void handle(ActionEvent event) {
        				vb.setBackground(unfocusBackground);
        				String pathToDelete = null;
        				int i=0;
        				boolean deleted = false;
        				Text filename = (Text)vb.getChildren().get(1);
        				String name = currentFolder+filename.getText();
        				File f = new File(name);
        				if(f.delete()) {
        					System.out.println("Deleted");
        					flowPane.getChildren().remove(vb);
        				}
        				}
        		});
        		move.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("Cut...");
                    }
                });
                copy.setOnAction(new EventHandler<ActionEvent>() {

        			@Override
        			public void handle(ActionEvent arg0) {
        				// TODO Auto-generated method stub
        				Stage stage = new Stage();
        				stage.initOwner(appStage);
        				stage.initModality(Modality.WINDOW_MODAL);
        				DirectoryChooser directoryChooser = new DirectoryChooser();
        		        File selectedDirectory = 
        		                 directoryChooser.showDialog(stage);
        		         
        		        if(selectedDirectory == null){
        		            //labelSelectedDirectory.setText("No Directory selected");
        		        }else{
        		            //labelSelectedDirectory.setText(selectedDirectory.getAbsolutePath());
        		        }
        		        //stage.show();
        			}
        		});
        	}
        	else {
        		System.out.println("Global effect");
        	}
        	contextMenu.show(parent, event.getScreenX(), event.getScreenY());
            event.consume();
        });
        vb.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
            contextMenu.hide();
        });
        //if user clicks elsewhere hide menu
        parent.addEventHandler(MouseEvent.MOUSE_PRESSED, event -> {
        	if (lastVb != null && parent.isFocused()) lastVb.setBackground(unfocusBackground);
            contextMenu.hide();
        });
	}
        
	@FXML
	private void showMenu(MouseEvent event) {
		VBox menuBox = new VBox();
		Button btn1 = new Button("Back");
		btn1.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				// TODO Auto-generated method stub
				fadeEffect(500, prevPane);
				mainPane.setLeft(prevPane);
			}
		});
		btn1.getStyleClass().add("my-button");
		btn1.setPrefWidth(179.0);
		Button btn2 = new Button("Option 2");
		btn2.getStyleClass().add("my-button");
		btn2.setPrefWidth(179.0);
		menuBox.getChildren().addAll(btn1,btn2);
		menuBox.setBackground(focusBackground);
		fadeEffect(500, menuBox);
		prevPane = mainPane.getLeft();
		mainPane.setLeft(menuBox);
	}
	
	private void fadeEffect(int duration,Node node) {
		FadeTransition ft = new FadeTransition(Duration.millis(duration), node);
		ft.setFromValue(0.0);
		ft.setToValue(1.0);
		ft.play();
	}
	@FXML
	private void prevImagePane() {
		System.out.println("Previous");
		Node prev = null;
		int i = 0;
		for (Node pane : imagePanes) {
			if (pane == flowPane) {
				currentFolder = folderPaths.get(i-1);
				folderPaths.remove(i);
				flowPane = (FlowPane)prev;
				imagePanes.remove(pane);
				fadeEffect(500, prev);
				filesPane.setContent(prev);
				if (currentFolder.equals(mainPath)) {
					logoutBackBtn.setText("Logout");
				}
				else {
					logoutBackBtn.setText("Back");
				}
				return;
			}
			prev = pane;
			i++;
		}
	}
	
	private void search() {
		System.out.println("Select first image that her name conatains search query!");
	}

	private void setGlobalEventHandler(Node node) {
	    node.addEventHandler(KeyEvent.KEY_PRESSED, ev -> {
	        if (ev.getCode() == KeyCode.ENTER) {
	        	if (node == search) {
	        		search();
				}
	           
	           ev.consume(); 
	        }
	    });
	}


public boolean deleteFile(String pathToDelete) {
		boolean retValue = deletePics(pathToDelete);
		if(retValue == true) {
			System.out.println("Photos were deleted");
			File dir = new File(pathToDelete);
			for (File f : dir.listFiles()) {
				if(!deleteFile(f.getAbsolutePath())) {
					retValue = false;
					break;
				}
			}
			if(!dir.delete()) {
				retValue = false;
			}
		}
		return retValue;
	}
	
	public boolean deletePics(String pathToDelete) {
		boolean retValue = true;
		File dir = new File(pathToDelete);
		for (File f : dir.listFiles()) {
			if(!f.isDirectory()) {
				if(!f.delete()) {
					retValue = false;
				}
			}
		}
		return retValue;
	}
}
